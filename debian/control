Source: golang-github-go-fed-httpsig
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Maytham Alsudany <maytha8thedev@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-golang-x-crypto-dev
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-go-fed-httpsig
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-go-fed-httpsig.git
Homepage: https://github.com/go-fed/httpsig
XS-Go-Import-Path: github.com/go-fed/httpsig

Package: golang-github-go-fed-httpsig-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-golang-x-crypto-dev,
         ${misc:Depends}
Description: HTTP signatures made simple (library)
 Implementation of HTTP Signatures (https://tools.ietf.org/html/draft-
 cavage-http-signatures).
 .
 Supports many different combinations of MAC, HMAC signing of hash, or
 RSA signing of hash schemes. Its goals are:
 .
  * Have a very simple interface for signing and validating
  * Support a variety of signing algorithms and combinations
  * Support setting either headers (Authorization or Signature)
  * Remaining flexible with headers included in the signing string
  * Support both HTTP requests and responses
  * Explicitly not support known-cryptographically weak algorithms
  * Support automatic signing and validating Digest headers
